// Contains instructions on how API will perfomr intended tasks
// All operatoinsit cando weill be plced it  thisfirel

const Task = require("../models/task");

// Controller function for getting all teh tasks
module.exports.getAllTasks = () => 
{
	return Task.find({}).then(result =>{
		return result;
	})
};

// Controller function for getting all teh tasks
module.exports.createTask = (requestBody) =>
{
	let newTask = new Task 
		({
			name: requestBody.name
		});

	return newTask.save().then((task,error) =>
	{
		if(error)
		{
			console.log(error);
			return false;
		}
		else
		{
			return task;
		}
	})
};

// Controller function for deleting a task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) =>
{
	return Task.findByIdAndRemove(taskId).then((removedTask,error) => {
		if(error)
		{
			console.log(error);
			return false;
		}
		else
		{
			return "Deleted Task"
		}
	})
}; 


// [SECTION] ACTIVITY START //

// #2 Controller function for getting specific task
module.exports.getOneTask = (taskId) =>
{
	// #3 Return result to client/postman
	return Task.findById(taskId).then(result =>{return result;});
}

// #6 Controller function for changing the status of task to complete
// NOTE: the sample output shows the body to be blank. For this reason, I opted to make the function forcefully change the status to complete without giving the user the option to freely update the document

module.exports.completeTask = (taskId) =>
{
	// #7 Return result to client/postman
	return Task.findByIdAndUpdate(taskId, {status: "complete"}, {new:true}).then(result =>{return result;});
}