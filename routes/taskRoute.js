// Defines when particular controllers will be used
// Contains all the endpoints and responses that we can get from controllers

const express = require("express");
// Creates a router instance that functions as a middleware and routing system
const router = express.Router();
module.exports = router;
const taskController = require("../controllers/taskController");

// Route to get all the tasks
// bec of index.js line 40, the current working endpoint is alr http://localhost:3001/task
// if we don't want a specific sublocation, a / will suffice for the endpoint
router.get("/", (req,res) =>
{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create task
router.post("/", (req,res) =>
{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Route to delete a task
router.delete("/:id", (req,res) =>
{
	taskController.deleteTask( req.params.id).then(resultFromController => res.send(resultFromController));
});


// [SECTION] ACTIVITY START //

// #1 Route to get specific task
router.get("/:id", (req,res) =>
{
	taskController.getOneTask( req.params.id).then(resultFromController => res.send(resultFromController));
});

// #5 Route to change status of task to complete
router.patch("/:id/complete", (req,res) =>
{
	taskController.completeTask( req.params.id).then(resultFromController => res.send(resultFromController));
});