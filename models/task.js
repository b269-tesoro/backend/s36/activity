// Contains objects that are needed in our API

const mongoose = require("mongoose");

// Create the schema, model, and export the file
const taskSchema = new mongoose.Schema({
	name: String,
	status: 
	{
		type: String,
		default: "pending"
	}
});

// module.exports is a awy for NodeJs to treat a value as a package that can be used by other files
module.exports = mongoose.model("Task", taskSchema);