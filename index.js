// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// this allows us to use all routes defined in taskRoute.js
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));



// Connecting to MongoDB Atlas START
	// replace <password> with db password, and place database name between / and ?
	mongoose.connect("mongodb+srv://tesorocelina:admin123@zuitt-bootcamp.ycfxf2m.mongodb.net/s36?retryWrites=true&w=majority",
		{
			// avoids current and future errors while connecting to MongoDB
			useNewUrlParser: true,
			useUnifiedTopology: true,
		}
	);

	mongoose.connection.once("open", () => console.log("Now connected to the cloud database."));
// Connecting to MongoDB Atlas END

// Allows all teh task routes created in the taskRoute.js file to use "/tasks" route
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}`));
